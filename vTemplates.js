/**
 * Class to manipulate Viptela templates.
 */
class vTemplates {
    constructor(vma) {
        this.vma = vma;
        this.dt = undefined;
        this.ft = undefined;
        this.verbose = 1;
    }

    login(user, pass) {
        var self = this;
        return self.vma.login(user, pass);
    }

    getData() {
        var self = this;
        return Promise.all([
            self.vma.get(GET_DEVICE_TEMPLATES),
            self.vma.get(GET_FEATURE_TEMPLATES)
        ]).then(function(values) {
            self.dt = JSON.parse(values[0][1]);
            self.ft = JSON.parse(values[1][1]);
        });
    }

    setDT(indt) {
        this.dt = indt;
    }

    setFT(inft) {
        this.ft = inft;
    }

    getDT() {
        //deep copy
        return JSON.parse(JSON.stringify(this.dt))
    }

    getFT() {
        return JSON.parse(JSON.stringify(this.ft))

    }

    listDeviceTemplates() {
        var textout = "";
        var dts = this.getDTsNonCLI();
        for (var i in dts) {
            textout += dts[i].templateName + "\n";
        }
        return textout;
    }

    listFeatureTemplates() {
        var textout = "";
        var fts = this.getFTsNonDefault();
        for (var i in fts) {
            textout += fts[i].templateName + "\n";
        }
        return textout;
    }

    listDeviceTemplateTreeView() {
        var textout = "";
        var dttree = this.getDTFTSorted();
        for (var i in dttree) {
            //looping over Device Templates
            textout += "\n" + dttree[i].templateName + "\n";
            for (var j in dttree[i].children) {
                //looping over children, this is flat, use level to figure
                //out hierarchy
                textout += "\t".repeat(dttree[i].children[j].level);
                textout += dttree[i].children[j].templateName + "\n";
            }
            textout;
        }
        return textout;
    }

    postDTs(inDT) {
        //inDT is the same format as this.dt.data
        //so should be similar to what is received via API under .data
        //you can pass this function getDTsNonCLI() if you 
        //want all device templates to be posted
        //http://stackoverflow.com/questions/31413749/node-js-promise-all-and-foreach
        var self = this;

        //create array of json data of device templates
        //we will POST each of these elements async
        var dts = [];
        var msg = "POSTing Device Templates to ";
        msg += self.vma.getBaseURL() + POST_DEVICE_TEMPLATE + ":";
        self.debug(msg);
        for (var i in inDT) {
            if (self.getDTIdByName(inDT[i].templateName) == undefined) {
                self.debug(inDT[i].templateName);
                dts.push(self.buildDeviceTemplate(inDT[i]));
            } else {
                self.debug(inDT[i].templateName + " - already exists");
            }
        }
        self.debug("");
        var fn = function(json) {
            return self.vma.post(POST_DEVICE_TEMPLATE, json);
        };
        var actions = dts.map(fn);
        return Promise.all(actions);
    }

    debug(msg) {
        if (this.verbose >= 1) {
            console.log(msg);
        }
    }

    postFTs(inFT) {
        var self = this;

        //create array of json data of device templates
        //we will POST each of these elements async
        var fts = [];
        var msg = "POSTing Feature Templates to ";
        msg += self.vma.getBaseURL() + POST_FEATURE_TEMPLATE + ":";
        self.debug(msg);
        for (var i in inFT) {
            if (self.getFTIdByName(inFT[i].templateName) == undefined) {
                self.debug(inFT[i].templateName);
                fts.push(self.buildFeatureTemplate(inFT[i]));
            } else {
                self.debug(inFT[i].templateName + " - already exists");
            }
        }
        self.debug("");
        var fn = function(json) {
            return self.vma.post(POST_FEATURE_TEMPLATE, json);
        };
        var actions = fts.map(fn);

        //feature templates have changed, re get data for this instance
        //since the next call will most likely be a device template build 
        //which will require the new uuid's associated with the new feature 
        //templates
        return Promise.all(actions);

    }

    getDTsNonCLI() {
        var self = this;
        var dtpruned = [];
        var dts = self.getDT();
        for (var i in dts.data) {
            //configType is file for CLI and template for device template
            //lets only work with device templates
            //add device templates to dtpruned
            if (dts.data[i].configType == "template") {
                dtpruned.push(dts.data[i]);
            }
        }
        return dtpruned;
    }

    getFTsNonDefault() {
        var self = this;
        var ftpruned = [];
        var fts = self.getFT();
        for (var i in fts.data) {
            if (fts.data[i].factoryDefault == false) {
                ftpruned.push(fts.data[i]);
            }
        }
        return ftpruned;
    }

    /**
     * Returns an array of Device Template objects with their associated 
     * feature template objects.
     */
    getDTFTSorted() {
        var data = this.getDTsNonCLI();
        var dttree = [];
        for (var i in data) {
            dttree.push({
                'templateName': data[i].templateName,
                'templateId': data[i].templateId,
                'children': []
            });
            var l = dttree.length;
            for (var j in data[i].generalTemplates) {
                this.DTFTSortedHelper(data[i].generalTemplates[j], 0, dttree[l - 1].children);
            }
        }
        return dttree;
    }

    DTFTSortedHelper(data, level, childNode) {
        level++;
        childNode.push({
            'templateName': data.templateName,
            'templateId': data.templateId,
            'level': level
        });
        if (data.subTemplates != undefined && data.subTemplates.length > 0) {
            for (var i in data.subTemplates) {
                this.DTFTSortedHelper(data.subTemplates[i], level, childNode);
            }
        }
    }

    getFTIdByName(name) {
        for (var i in this.ft.data) {
            if (this.ft.data[i].templateName == name) {
                return this.ft.data[i].templateId;
            }
        }
    }

    getDTIdByName(name) {
        for (var i in this.dt.data) {
            if (this.dt.data[i].templateName == name) {
                return this.dt.data[i].templateId;
            }
        }
    }

    buildDTHelper(data) {
        data.templateId = this.getFTIdByName(data.templateName);
        if (data.subTemplates != undefined && data.subTemplates.length > 0) {
            for (var i in data.subTemplates) {
                this.buildDTHelper(data.subTemplates[i]);
            }
        }
    }

    buildDeviceTemplate(srcTemplateData) {
        /**
         * generalTemplates field below cannot be a direct copy from source 
         * device template.  General templates contains UUIDs specific to each 
         * vmanage.  We need to go through each one by name and match it to the 
         * feature template on the destination vmanage.
         * 
         */
        var td = srcTemplateData;
        for (var i in td.generalTemplates) {
            this.buildDTHelper(td.generalTemplates[i]);
        }

        var data = {
            "deviceType": srcTemplateData.deviceType,
            "factoryDefault": false,
            "configType": srcTemplateData.configType,
            "featureTemplateUidRange": [],
            "policyId": "",
            "templateDescription": srcTemplateData.templateDescription,
            "templateName": srcTemplateData.templateName,
            "generalTemplates": srcTemplateData.generalTemplates
        };
        return data;
    }

    buildFeatureTemplate(srcTemplateData) {
        var data = {
            "deviceType": srcTemplateData.deviceType,
            "factoryDefault": false,
            "templateDefinition": JSON.parse(srcTemplateData.templateDefinition),
            "templateDescription": srcTemplateData.templateDescription,
            "templateMinVersion": srcTemplateData.templateMinVersion,
            "templateName": srcTemplateData.templateName,
            "templateType": srcTemplateData.templateType
        };
        return data;
    }
}

GET_FEATURE_TEMPLATES = 'dataservice/template/feature';
GET_DEVICE_TEMPLATES = 'dataservice/template/device';

POST_DEVICE_TEMPLATE = 'dataservice/template/device/feature';
POST_FEATURE_TEMPLATE = 'dataservice/template/feature';

module.exports = vTemplates;