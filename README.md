vtemplates-cli
=========

Import and export vManage Templates from the command line.


How to install
---

If you haven't already got Node.js, then [go get it](http://nodejs.org/).
Then:

```
git clone https://pcassell@bitbucket.org/pcassell/vtemplates-cli.git
cd vtemplates-cli
npm install
```


Running
---

vtcli.js will copy all non-cli device templates and non-default feature templates from a source to a destination.  If any template name already exist, that template will be skipped.

```
node vtcli.js [source] [destination]
```

Where [source] or [destination] can be 

http://1.2.3.4:8443
 
file:/path/to/file.json

If [destination] is omitted will list tree view of Device Templates


Examples
---

Export all feature templates and device templates from one vmanage to another
```
node vtcli.js https://147.75.108.45:9999 https://147.75.203.193:9999
```

Export all feature templates and device templates from one vmanage to a local file backup.json
```
node vtcli.js https://147.75.108.45:9999 file:backup.json
```

List all feature templates and device templates in the file backup.json
```
node vtcli.js file:backup.json
```


FAQs
----

### nope