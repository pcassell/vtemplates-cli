var request = require('request');

/**
 * Class to manage access to vManage REST API.
 */
class vManageAccess {
    constructor(server) {
        this.baseurl = 'https://' + server + '/';
        this.loginAction = 'j_security_check'
        this.j = request.jar();
    }

    getBaseURL() {
        return this.baseurl;
    }

    login(user, pass) {
        this.user = user;
        this.pass = pass;
        var self = this;
        return new Promise(function(resolve, reject) {
            var logindata = {
                'j_username': self.user,
                'j_password': self.pass
            };
            request.post({
                url: self.baseurl + self.loginAction,
                form: logindata,
                jar: self.j,
                rejectUnauthorized: false
            }, function(error, response, body) {
                if (error) {
                    return reject(error);
                } else if (body.includes("<html>")) {
                    //showing login prompt, bad user pass
                    return reject(new Error("Login failed, check username and password"));
                } else {
                    resolve([response.statusCode, body]);
                }
            });
        });
    }

    get(path) {
        var self = this;
        return new Promise(function(resolve, reject) {
            request.get({
                url: self.baseurl + path,
                jar: self.j,
                rejectUnauthorized: false
            }, function(error, response, body) {
                if (error) {
                    return reject(error);
                } else {
                    resolve([response.statusCode, body]);
                }
            });
        });
    }

    post(path, data) {
        //json parameter expects data in non text format so no need to 
        //stringify first
        var self = this;
        return new Promise(function(resolve, reject) {
            request.post({
                url: self.baseurl + path,
                json: data,
                jar: self.j,
                rejectUnauthorized: false
            }, function(error, response, body) {
                if (error) {
                    return reject(error);
                } else {
                    resolve([response.statusCode, body]);
                }
            });
        });
    }

}

module.exports = vManageAccess;