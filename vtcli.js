var argv = require('minimist')(process.argv.slice(2));
var request = require('request');
var prompt = require('prompt');
var fs = require('fs');
var vManageAccess = require("./vManageAccess.js");
var vTemplates = require("./vTemplates.js");

//hold promise
var srcP;
var dstP;

//setup access to source
var srcType = fileOrHTTP(argv['_'][0]);
if (srcType[0] == "file") {
    //source is file:
    var srcVMA = new vManageAccess();
    var srcTemplates = new vTemplates();
    //load json from file
    srcP = getJSONFromFile(srcType[1])
        .then(function(json) {
            srcTemplates.setFT(json.ft);
            srcTemplates.setDT(json.dt);
            console.log("Read JSON in from file:  " + srcType[1]);
        })
        .catch(function(e) {
            debug(e);
            console.log(e.message);
            process.exit(1);
        });
} else {
    //source is https://
    var srcVMA = new vManageAccess(srcType[1]);
    var srcTemplates = new vTemplates(srcVMA);
    //load json from vmanage
    //add as first promise getUserPass, then login, then getData
    var msg = "Enter login credentials for vManage @ " + argv['_'][0];
    srcP = getUserPass(msg)
        .then(function(userpass) {
            return srcVMA.login(userpass['Username'], userpass['Password']);
        })
        .then(function(value) {
            return srcTemplates.getData();
        })
        .catch(function(e) {
            debug(e);
            console.log(e.message);
            process.exit(1);
        });
}

//setup access to destination, if added to CLI
var destFlag = false;
var dstType;
var dstVMA;
var dstTemplates;
if (argv['_'][1] != undefined) {
    dstType = fileOrHTTP(argv['_'][1]);
    destFlag = true;
    if (dstType[0] == "file") {
        //destination is file:
        if (srcType[0] == "file") {
            console.log("Nothing to do.  Source and destination are both file:");
            process.exit(1);
        }
        srcP = srcP.then(function(value) {
                var data = {};
                data.ft = srcTemplates.getFT();
                data.dt = srcTemplates.getDT();
                return writeJSONToFile(dstType[1], data)
            })
            .then(function(value) {
                console.log("Wrote JSON to file:  " + dstType[1]);
                process.exit(0);
            })
            .catch(function(e) {
                debug(e);
                console.log(e.message);
                process.exit(1);
            });
    } else {
        //destination is https://
        dstVMA = new vManageAccess(dstType[1]);
        dstTemplates = new vTemplates(dstVMA);
        //load json from vmanage
        //add as first promise getUserPass, then login, then getData
        var msg = "Enter login credentials for vManage @ " + argv['_'][1];
        srcP = srcP.then(function(value) {
                return getUserPass(msg);
            })
            .then(function(userpass) {
                return dstVMA.login(userpass['Username'], userpass['Password']);
            })
            .then(function(value) {
                return dstTemplates.getData();
            })
            .catch(function(e) {
                debug(e);
                console.log(e.message);
                process.exit(1);
            });
    }
}

//if no destination, just list from file or http
if (destFlag == false) {
    srcP = srcP.then(function(value) {
        console.log(srcTemplates.listDeviceTemplateTreeView());
        process.exit(0);
    });
}

//otherwise, we can copy from source to dest
srcP = srcP.then(function(value) {
        //var obj = srcTemplates.getDTFTSorted();
        //console.log(util.inspect(obj, false, null))
        var ft = srcTemplates.getFTsNonDefault();
        return dstTemplates.postFTs(ft);
    })
    .then(function(value) {
        reportStatus(value);
        //re get data since FT's added and new uuids
        return dstTemplates.getData();
    })
    .then(function(value) {
        //console.log(value);
        var dt = srcTemplates.getDTsNonCLI();
        return dstTemplates.postDTs(dt);
    })
    .then(function(value) {
        reportStatus(value);
        process.exit(0);
    }).catch(function(e) {
        debug(e);
        console.log(e.message);
        process.exit(1);
    });

function debug(msg) {
    if (argv['v'] == true) {
        console.log(msg);
    }
}

function reportStatus(value) {
    for (var i in value) {
        if (value[i][0] != 200) {
            console.log(value[i][0]);
            console.log(value[i][1]);
        }
    }
}

function getUserPass(msg) {
    return new Promise(function(resolve, reject) {
        prompt.message = '';
        prompt.start();
        console.log(msg);
        prompt.get([{
            name: 'Username',
            required: true
        }, {
            name: 'Password',
            hidden: true,
            conform: function(value) {
                return true;
            }
        }], function(err, result) {
            if (err) {
                reject(Error(err));
            }
            resolve(result);
        });
    });
}

function usage(e) {
    var usage = "\nusage: vtcli.js [source] [destination] \n\n";
    usage += "Where [source] or [destination] can be 'http://1.2.3.4:8443' ";
    usage += "or 'file:/path/to/file.json'\n";
    usage += "If [destination] is ommitted will list tree view of Device Templates\n";
    console.log(usage);
    process.exit(e);
}

function fileOrHTTP(text) {
    var file = isFile(text);
    var http = isHTTP(text);
    var retval = [];
    if (file != null) {
        //file
        retval.push("file");
        retval.push(file);
    } else if (http != null) {
        //http
        retval.push("http");
        retval.push(http);
    } else {
        //not a file or http
        usage(1);
    }
    return retval;
}

function isFile(text) {
    var regex = /^file:(.+)/;
    var result = text.match(regex);
    if (result != null) {
        //file: found at the front of text
        return result[1];
    }
}

function isHTTP(text) {
    var regex = /^http[s]{0,1}:\/\/(.+)/;
    var result = text.match(regex);
    if (result != null) {
        //http(s):// found at the front of text
        return result[1];
    }
}

function getJSONFromFile(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename, function(error, data) {
            if (error) {
                return reject(error);
            } else {
                resolve(JSON.parse(data.toString()));
            }
        });
    });
}

function writeJSONToFile(filename, data) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(filename, JSON.stringify(data), { flag: "wx" }, function(error) {
            if (error) {
                return reject(error);
            } else {
                resolve();
            }
        });
    });
}